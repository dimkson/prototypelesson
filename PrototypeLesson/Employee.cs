﻿using System;

namespace PrototypeLesson
{
    public class Employee : Person, IMyCloneable<Person>, ICloneable
    {
        public string Title;
        public decimal Salary;

        public Employee(string name, int age, DateTime birthDate, Address address, string title, decimal salary) 
            : base(name, age, birthDate, address)
        {
            Title = title;
            Salary = salary;
        }

        public Employee(Employee source) : base(source)
        {
            Title = source.Title;
            Salary = source.Salary;
        }

        public override object Clone()
        {
            return DeepCopy();
        }

        public override Person DeepCopy()
        {
            return new Employee(this);
        }

        public override string ToString()
        {
            return $"{base.ToString()} {Title} {Salary}";
        }
    }
}

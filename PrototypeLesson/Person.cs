﻿using System;

namespace PrototypeLesson
{
    public class Person : IMyCloneable<Person>, ICloneable
    {
        public string Name;
        public int Age;
        public DateTime BirthDate;
        public Address Address;

        public Person(string name, int age, DateTime birthDate, Address address)
        {
            Name = name;
            Age = age;
            BirthDate = birthDate;
            Address = address;
        }
        public Person(Person source)
        {
            Name = source.Name;
            Age = source.Age;
            BirthDate = source.BirthDate;
            Address = new Address(source.Address);
        }

        public virtual object Clone()
        {
            return DeepCopy();
        }

        public virtual Person DeepCopy()
        {
            return new Person(this);
        }

        public override string ToString()
        {
            return $"{Name} {Age} {BirthDate} {Address}";
        }
    }
}

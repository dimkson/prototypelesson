﻿using System;

namespace PrototypeLesson
{
    public class Address : IMyCloneable<Address>, ICloneable
    {
        public string Street;
        public string City;
        public int HouseNumber;

        public Address(string city, string street, int houseNumber)
        {
            City = city;
            Street = street;
            HouseNumber = houseNumber;
        }

        public Address(Address source)
        {
            Street = source.Street;
            City = source.City;
            HouseNumber = source.HouseNumber;
        }

        public Address DeepCopy()
        {
            return new Address(this);
        }

        public object Clone()
        {
            return DeepCopy();
        }

        public override string ToString()
        {
            return $"City:{City}, Street: {Street}, House:{HouseNumber}";
        }
    }
}
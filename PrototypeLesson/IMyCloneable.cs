﻿namespace PrototypeLesson
{
    internal interface IMyCloneable<T>
    {
        T DeepCopy();
    }
}

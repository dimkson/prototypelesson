using System;
using Xunit;
using PrototypeLesson;

namespace PrototypeLessonTests
{
    public class PrototypeTests
    {
        private Address _address;
        private Person _person;
        private Employee _employee;

        public PrototypeTests()
        {
            _address = new Address("Moscow", "First street", 238);
            _person = new Person("Tom", 28, DateTime.Now, _address);
            _employee = new Employee("Tom", 28, DateTime.Now, _address, "Manager", 10);
        }

        [Fact]
        [Trait("Tests","Address")]
        public void CloneAddressEqualsOriginAddress()
        {
            var clone = _address.DeepCopy();

            Assert.Equal(_address.ToString(), clone.ToString());
        }

        [Fact]
        [Trait("Tests", "Address")]
        public void CloneAddressDontChangeOriginAddress()
        {
            var deepCopy = _address.DeepCopy();
            deepCopy.City = "Spb";
            deepCopy.Street = "Second street";
            deepCopy.HouseNumber = 100;

            Assert.Equal("Spb", deepCopy.City);
            Assert.Equal("Moscow", _address.City);
            Assert.Equal("Second street", deepCopy.Street);
            Assert.Equal("First street", _address.Street);
            Assert.Equal(100, deepCopy.HouseNumber);
            Assert.Equal(238, _address.HouseNumber);
        }

        [Fact]
        [Trait("Tests", "Person")]
        public void ClonePersonEqualsOriginPerson()
        {
            var deepCopy = _person.DeepCopy();

            Assert.Equal(_person.ToString(), deepCopy.ToString());
        }

        [Fact]
        [Trait("Tests", "Person")]
        public void ClonePersonDontChangeOriginPerson()
        {
            var deepCopy = _person.DeepCopy();
            deepCopy.Name = "Sam";
            deepCopy.Age = 30;
            deepCopy.Address = new Address("Spb", "Second street", 100);

            Assert.Equal("Sam", deepCopy.Name);
            Assert.Equal("Tom", _person.Name);
            Assert.Equal(30, deepCopy.Age);
            Assert.Equal(28, _person.Age);
            Assert.Equal("Spb", deepCopy.Address.City);
            Assert.Equal("Moscow", _person.Address.City);
        }

        [Fact]
        [Trait("Tests", "Employee")]
        public void CloneEmployeeEqualsOriginEmployee()
        {
            var deepCopy = _employee.DeepCopy();

            Assert.Equal(_employee.ToString(), deepCopy.ToString());
        }

        [Fact]
        [Trait("Tests", "Employee")]
        public void CloneEmployeeDontChangeOriginEmployee()
        {
            var deepCopy = _employee.DeepCopy() as Employee;
            deepCopy.Name = "Sam";
            deepCopy.Age = 30;
            deepCopy.Address = new Address("Spb", "Second street", 100);
            deepCopy.Title = "IT";
            deepCopy.Salary = 100;

            Assert.Equal("Sam", deepCopy.Name);
            Assert.Equal("Tom", _employee.Name);
            Assert.Equal(30, deepCopy.Age);
            Assert.Equal(28, _employee.Age);
            Assert.Equal("Spb", deepCopy.Address.City);
            Assert.Equal("Moscow", _employee.Address.City);
            Assert.Equal("IT", deepCopy.Title);
            Assert.Equal("Manager", _employee.Title);
            Assert.Equal(100, deepCopy.Salary);
            Assert.Equal(10, _employee.Salary);
        }
    }
}
